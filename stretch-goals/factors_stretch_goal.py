def factors(number):
    # ==============
    # Your code here
    factor = []
    for n in range(2, number):

        if number % n == 0:
            factor.append(n)

    if factor.__len__() == 0:
        output = str(number) + ' is a prime number'
        return output

    return factor
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
