def vowel_swapper(string):
    # ==============
    # Your code here

    vowel_dict = {
        'A': '4',
        'a': '4',
        'E': '3',
        'e': '3',
        'I': '!',
        'i': '!',
        'O': '000',
        'o': 'ooo',
        'U': '|_|',
        'u': '|_|'
    }

    vowel_count = {
        'a': 0,
        'e': 0,
        'i': 0,
        'o': 0,
        'u': 0
    }

    out_string = ''

    for char in string:
        for key in vowel_dict:

            if char.find(key) != -1:
                vowel_count[key.lower()] += 1

                if vowel_count[key.lower()] == 2:
                    char = vowel_dict[char]
                    break

        out_string += char

    return out_string
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a4a e3e i!i o000o u|_|u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av4!lable" to the console
